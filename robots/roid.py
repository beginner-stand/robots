from main import app
from flask import request
import re
robot_pos = {}
idre = re.compile(r'\d{1,6}')

@app.route('/robot/<ro_id>/position', methods=['PUT'])
def robotposput(ro_id):
    if not idre.fullmatch(ro_id):
        return {},400
    ro_id = int(ro_id)
    if not 1 <= ro_id <= 999999:
        return {},400
    pos = request.json['position']
    if not 'x' in pos or not isinstance(pos['x'],int):
        return {},400
    if not 'y' in pos or not isinstance(pos['y'],int):
        return {},400
    robot_pos[ro_id] = {"x" :pos["x"] ,"y":pos["y"]}
    return {},204

@app.route('/robot/<ro_id>/position', methods=['GET'])
def robotposget(ro_id):
    if not idre.fullmatch(ro_id):
        return {},400
    ro_id = int(ro_id)
    if not 1 <= ro_id <= 999999:
        return {},400
    if ro_id in robot_pos:
        return {'position':{"x" : robot_pos[ro_id]['x'], "y": robot_pos[ro_id]["y"]} }
    else:
        return {},404
