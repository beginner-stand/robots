from main import app
from flask import request
import re
from roid import robot_pos as robot_pos
robotre = re.compile(r'^robot#([1-9][0-9]*$)')


@app.route('/distance', methods=['POST'])
def dista():
	if not request.is_json:
		return {}, 400
	if 'first_pos' not in request.json:
		return {}, 424
	if 'second_pos' not in request.json:
		return {}, 424
	pos1 = request.json['first_pos']
	pos2 = request.json['second_pos']
	if 'metric' not in request.json:
		metric = 'euclidean'
	else:
		metric = request.json['metric']
	if robotre.fullmatch(str(pos1)):
		rob = robotre.search(str(pos1))
		ro_id = int(rob.group(1))
		if ro_id not in robot_pos:
			return {},424
		pos1 = robot_pos[ro_id]
	if robotre.fullmatch(str(pos2)):
		rob = robotre.search(str(pos2))
		ro_id = int(rob.group(1))
		if ro_id not in robot_pos:
			return {},424
		pos2 = robot_pos[ro_id]
	if 'x' not in pos1 or 'x' not in pos2:
		return {}, 424
	if not isinstance(pos1['x'],int) or not isinstance(pos2['x'],int):
		return {}, 400
	dx = pos1['x']-pos2['x']
	if 'y' not in pos1 or 'y' not in pos2:
		return {}, 424
	if not isinstance(pos1['y'],int) or not isinstance(pos2['y'],int):
		return {}, 400
	dy = pos1['y']-pos2['y']
	if metric == 'euclidean':
		dist = ((dx)**2 + (dy)**2)**(1/2)
	elif metric == 'manhattan':
		dist = abs(dx) + abs(dy)
	return {"distance" : dist}
