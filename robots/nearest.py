from main import app
from flask import request
from roid import robot_pos

def helper_dist(p1, p2):
	dx = p1[0] - p2[0]
	dy = p1[1] - p2[1]
	return dx ** 2 + dy ** 2

def comp(r):
	return helper_dist((x,y), (rx,ry)) 

@app.route('/nearest', methods=['POST'])
def nearest():
	if not request.is_json:
		return {}, 400
	if 'ref_position' not in request.json:
		return {}, 400
	k = 1
	if 'k' in request.json:
		k = request.json['k']
	rpos = request.json['ref_position']
	if 'x' not in rpos:
		return {}, 400
	if 'y' not in rpos:
		return {}, 400
	x = rpos['x']
	y = rpos['y']
	rawrob = []
	for robot in sorted(robot_pos):
		rx = robot_pos[robot]['x']
		ry = robot_pos[robot]['y']
		rawrob.append((robot,rx,ry))
	rawrob.sort(key=lambda r: helper_dist((x,y), r[1:]))
	retr = list(map(lambda out: out[0], rawrob))
	return {"robot_ids": retr[:k]}

