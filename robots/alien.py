from main import app
from flask import request
import re
from roid import robot_pos as robot_pos
alienre = re.compile(r'^[a-z]{1,16}$')
idre = re.compile(r'\d{1,6}')
alien_dict = {}

def fdis(x1,x2,y1,y2):
	dx = x1 -x2
	dy = y1 - y2
	return (dx ** 2 + dy ** 2)**0.5

@app.route('/alien/<name>/report', methods=['POST'])
def reportalien(name):
    if 'robot_id' not in request.json:
        return {},400
    if 'distance' not in request.json:
        return {},400
    if not alienre.fullmatch(name):
        return {},400
    ro_id = request.json["robot_id"]
    dis = request.json["distance"]
    if ro_id not in robot_pos:
        return {},400
    if name not in alien_dict:
        alien_dict[name] = {}
    alien_dict[name][ro_id] = {"id" : ro_id,"dis" : dis}
    return {}

@app.route('/alien/<name>/position', methods=['GET'])
def posalien(name):
    if not alienre.fullmatch(name):
        return {},424
    if name not in alien_dict:
        return {},424
    if len(alien_dict)<2:
        return {},424
    run = 0
    for data in alien_dict[name]:
        if run ==0:
            r0 = data["dis"]
            x0 = robot_pos[data["id"]]['x']
            y0 = robot_pos[data["id"]]['y']
        if run == 1:
            r1 = data["dis"]
            x1 = robot_pos[data["id"]]['x']
            y1 = robot_pos[data["id"]]['y']
            r0 = 2.236
            d=((x1-x0)**2 + (y1-y0)**2)**(1/2)
            a=(r0**2-r1**2+d**2)/(2*d)
            h=(r0**2-a**2)**(1/2)
            x2=x0+a*(x1-x0)/d   
            y2=y0+a*(y1-y0)/d   
            x3=x2+h*(y1-y0)/d 
            y3=y2-h*(x1-x0)/d
            ch2x = False
            ch2y = False
            ch3x = False
            ch3y = False
            if abs(x2 - int(x2))<0.001:
                ch2x = True
                x2 = int(x2)
            if abs(y2 - int(y2))<0.001:
                ch2y = True
                y2 = int(y2)
            if abs(x3 - int(x3))<0.001:
                ch3x = True
                x3 = int(x3)
            if abs(y3 - int(y3))<0.001:
                ch3y = True
                y3 = int (y3)
            ch2 = ch2x and ch2y
            ch3 = ch3x and ch3y
            if ch2 and not ch3:
                return {"position":{"x":x2,"y":y2}}
            if not ch2 and ch3:
                return {"position":{"x":x3,"y":y3}}
            if not ch2 and not ch3:
                return {},424
        if run>2:
            r0 = data["dis"]
            x0 = robot_pos[data["id"]]['x']
            y0 = robot_pos[data["id"]]['y']
            if abs(fdis(x0,x2,y0,y2)-r0)<0.001:
                return {"position":{"x":x2,"y":y2}}
            if abs(fdis(x0,x3,y0,y3)-r0)<0.001:
                return {"position":{"x":x3,"y":y3}}
        i+=1